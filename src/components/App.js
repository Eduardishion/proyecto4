import React, { Component } from 'react'
import Header from './Header'
import Formulario from './Formulario'
import Error from './Error'
import Clima from './Clima'
import '../css/App.css'

export default class App extends Component {
  state = {
    error: '',
    consulta: {},
    resultado: {}
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.consulta !== this.state.consulta) {
      this.consultarApi()
    }
  }

  componentDidMount() {
    this.setState({
      error: false
    })
  }

  datosConsulta = respuesta => {
    respuesta.ciudad === '' || respuesta.pais === ''
      ? this.setState({
          error: true
        })
      : this.setState({
          error: false,
          consulta: respuesta
        })
  }

  consultarApi = () => {
    const { ciudad, pais } = this.state.consulta
    if (!ciudad || !pais) return null

    const APPID = '472b6baafe8ffa76df521fdf39a74050'
    let APIURL = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&APPID=${APPID}`

    fetch(APIURL)
      .then(respuesta => {
        return respuesta.json()
      })
      .then(datos => {
        this.setState({
          resultado: datos
        })
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    let { cod } = this.state.resultado
    const error = this.state.error
    let resultado

    if (error) {
      resultado = <Error mensaje="Ambos campos son obligatorios" />
    } else if (cod === '404') {
      resultado = <Error mensaje="La ciudad no se encuentra" />
    } else {
      resultado = <Clima resultado={this.state.resultado} />
    }

    return (
      <div className="App">
        <Header titulo="Clima React" />
        <Formulario datosConsulta={this.datosConsulta} />
        {resultado}
      </div>
    )
  }
}
